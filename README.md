Installation
---------------------------

1. `git clone https://bitbucket.org/esazykin/comments-system.git`

2. `composer install`

3. `copy ./config/nginx/conf.example ./config/nginx/conf` and configure

4. `copy ./config/autoload/local.php.dist ./config/autoload/local.php` and configure

5. create db "comments_system" and run our migrations `./vendor/bin/phinx migrate`

6. include ./config/nginx/conf.example file to nginx config and restart it

7. `cd public_resource` and `npm install`

8. `npm run dev` for developing or `npm run build`