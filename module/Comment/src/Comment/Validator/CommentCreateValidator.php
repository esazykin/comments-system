<?php

namespace Comment\Validator;

use Zend\InputFilter\InputFilter;

class CommentCreateValidator extends InputFilter
{
    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->add([
            'name'     => 'title',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 50,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name'     => 'text',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 1000,
                    ],
                ],
            ],
        ]);
    }
}