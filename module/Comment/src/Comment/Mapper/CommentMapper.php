<?php

namespace Comment\Mapper;

use Comment\Model\CommentInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\ResultSet\Exception\InvalidArgumentException as DbInvalidArgumentException;
use Zend\Db\Sql\Exception\InvalidArgumentException;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Hydrator\HydratorInterface;

class CommentMapper implements CommentMapperInterface
{
    /**
     * @var AdapterInterface
     */
    protected $dbAdapter;

    /**
     * @var HydratorInterface
     */
    protected $hydrator;

    /**
     * @var Sql
     */
    protected $sql;

    /**
     * @var CommentInterface
     */
    protected $commentPrototype;

    public function __construct(
        AdapterInterface $dbAdapter,
        HydratorInterface $hydrator,
        CommentInterface $commentPrototype
    )
    {
        $this->dbAdapter = $dbAdapter;
        $this->hydrator = $hydrator;
        $this->commentPrototype = $commentPrototype;
        $this->sql = new Sql($this->dbAdapter, 'comment');
    }

    /**
     * @param int|string $id
     * @return CommentInterface
     * @throws InvalidArgumentException
     */
    public function find($id)
    {
        $select = $this->sql->select()
            ->where(['id = ?' => $id]);

        $stmt = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if ($result instanceof ResultInterface && $result->isQueryResult() && $result->getAffectedRows()) {
            return $this->hydrator->hydrate($result->current(), $this->commentPrototype);
        }

        throw new InvalidArgumentException("Object #$id not found");
    }

    /**
     * @param Select|null $select
     * @return null|\Zend\Db\ResultSet\ResultSet
     * @throws InvalidArgumentException | DbInvalidArgumentException
     */
    public function findAll(Select $select = null)
    {
        if ($select) {
            $select->from($this->sql->getTable());
        } else {
            $select = $this->sql->select();
        }
        $stmt = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            $resultSet = new HydratingResultSet($this->hydrator, $this->commentPrototype);

            return $resultSet->initialize($result);
        }
        return null;
    }

    /**
     * @param array $data
     * @return CommentInterface
     * @throws \Exception | InvalidArgumentException
     */
    public function create(array $data)
    {
        unset($data['id']);
        $data['created'] = (new \DateTime())->format('Y-m-d H:i:s');

        $insert = (new Insert($this->sql->getTable()))->values($data);
        $stmt = $this->sql->prepareStatementForSqlObject($insert);
        $result = $stmt->execute();
        if ($result instanceof ResultInterface) {
            $data['id'] = $result->getGeneratedValue();
            if ($data['id']) {
                return $this->hydrator->hydrate($data, $this->commentPrototype);
            }
        }

        throw new \Exception("Database error");
    }
}