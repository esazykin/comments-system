<?php

namespace Comment\Mapper\Sql;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Select;

class CommentSelect extends Select
{
    const MODE_ALL = 0;
    const MODE_NEW = 1;
    const MODE_RAND = 2;
    
    public function setMode($mode)
    {
        switch ($mode) {
            case self::MODE_RAND:
                $this->order(new Expression('RAND()'));
                break;
            case self::MODE_NEW:
                $this->where(new Operator(
                    'created',
                    Operator::OPERATOR_GREATER_THAN_OR_EQUAL_TO,
                    new Expression('DATE_SUB(NOW(), INTERVAL 1 MINUTE)')
                ));
                $this->order(['id' => self::ORDER_DESCENDING]);
                break;
            case self::MODE_ALL:
            default:
                $this->order(['id' => self::ORDER_ASCENDING]);
        }
        return $this;
    }
}