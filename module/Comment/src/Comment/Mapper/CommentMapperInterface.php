<?php

namespace Comment\Mapper;

use Comment\Model\CommentInterface;
use Zend\Db\ResultSet\Exception\InvalidArgumentException as DbInvalidArgumentException;
use Zend\Db\Sql\Exception\InvalidArgumentException;
use Zend\Db\Sql\Select;

interface CommentMapperInterface
{
    /**
     * @param int|string $id
     * @return CommentInterface
     * @throws InvalidArgumentException
     */
    public function find($id);

    /**
     * @param Select|null $select
     * @return null|\Zend\Db\ResultSet\ResultSet
     * @throws InvalidArgumentException | DbInvalidArgumentException
     */
    public function findAll(Select $select = null);

    /**
     * @param array $data
     * @return CommentInterface
     * @throws \Exception | InvalidArgumentException
     */
    public function create(array $data);
}