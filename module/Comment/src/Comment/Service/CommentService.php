<?php

namespace Comment\Service;

use Comment\Exception\CreateCommentException;
use Comment\Mapper\CommentMapperInterface;
use Comment\Model\CommentInterface;
use Comment\Validator\CommentCreateValidator;
use Comment\Exception\ValidationException;
use Zend\Db\ResultSet\Exception\InvalidArgumentException as DbInvalidArgumentException;
use Zend\Db\Sql\Exception\InvalidArgumentException;
use Zend\Db\Sql\Select;
use Zend\InputFilter\Exception\InvalidArgumentException as InputFilterInvalidArgumentException;

class CommentService implements CommentServiceInterface
{
    /**
     * @var CommentMapperInterface
     */
    protected $mapper;

    public function __construct(CommentMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param  int $id
     * @return CommentInterface
     * @throws InvalidArgumentException
     */
    public function find($id)
    {
        return $this->mapper->find($id);
    }

    /**
     * @param Select|null $select
     * @return null|\Zend\Db\ResultSet\ResultSet
     * @throws InvalidArgumentException | DbInvalidArgumentException
     */
    public function findAll(Select $select = null)
    {
        return $this->mapper->findAll($select);
    }

    /**
     * @param array $data
     * @return CommentInterface
     * @throws ValidationException | CreateCommentException
     */
    public function create(array $data)
    {
        try {
            $validator = (new CommentCreateValidator())->setData($data);
        } catch (InputFilterInvalidArgumentException $e) {
             throw (new ValidationException())->setErrors(['external' => ['Validation error']]);
        }
        if (!$validator->isValid()) {
            throw (new ValidationException())->setErrors(['external' => $validator->getMessages()]);
        }
        try {
            return $this->mapper->create($validator->getValues());
        } catch (\Exception $e) {
            throw (new CreateCommentException())->setErrors(['internal' => 'Creation error']);
        }
    }
}