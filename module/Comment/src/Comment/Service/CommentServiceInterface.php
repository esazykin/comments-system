<?php

namespace Comment\Service;

use Comment\Exception\CreateCommentException;
use Comment\Exception\ValidationException;
use Comment\Model\CommentInterface;
use Zend\Db\ResultSet\Exception\InvalidArgumentException as DbInvalidArgumentException;
use Zend\Db\Sql\Exception\InvalidArgumentException;
use Zend\Db\Sql\Select;

interface CommentServiceInterface
{
    /**
     * @param  int $id
     * @return CommentInterface
     * @throws InvalidArgumentException
     */
    public function find($id);
    
    /**
     * @param Select|null $select
     * @return null|\Zend\Db\ResultSet\ResultSet
     * @throws InvalidArgumentException | DbInvalidArgumentException
     */
    public function findAll(Select $select = null);

    /**
     * @param array $data
     * @return CommentInterface
     * @throws ValidationException | CreateCommentException
     */
    public function create(array $data);
}