<?php

namespace Comment\Model;

interface CommentInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getText();
    
    /**
     * @param int $id
     */
    public function setId($id);
    
    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @param string $text
     */
    public function setText($text);
    
    /**
     * @return string
     */
    public function getCreated();

    /**
     * @param mixed $created
     */
    public function setCreated($created);
}