<?php

namespace Comment\Controller;

use Comment\Mapper\Sql\CommentSelect;
use Comment\Model\CommentInterface;
use Comment\Service\CommentServiceInterface;

class CommentController extends BaseRestController
{
    const RESOURCE_INTERFACE = CommentInterface::class;
    
    /**
     * @var CommentServiceInterface
     */
    protected $commentService;

    public function __construct(CommentServiceInterface $commentService)
    {
        $this->commentService = $commentService;
    }

    public function get($id)
    {
        return $this->process(function() use ($id) {
            return $this->commentService->find($id);
        });
    }
    
    public function getList()
    {
        return $this->process(function() {
            $mode = $this->params()->fromQuery('mode');
            return $this->commentService->findAll((new CommentSelect())->setMode($mode));
        });
    }

    public function create($data)
    {
        return $this->process(function() use ($data) {
            return $this->commentService->create($data);
        });
    }
}