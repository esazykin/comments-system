<?php

namespace Comment\Controller;

use Comment\Exception\BaseException;
use Zend\Hydrator\ClassMethods;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

abstract class BaseRestController extends AbstractRestfulController
{
    const RESOURCE_INTERFACE = null;
    
    /**
     * Create a new resource
     *
     * @param mixed $data
     * @return JsonModel
     */
    public function create($data)
    {
        return new JsonModel(parent::create($data));
    }

    /**
     * Create a new resource
     *
     * @param mixed $id
     * @return JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(parent::delete($id));
    }

    /**
     * Delete the entire resource collection
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param $data
     * @return JsonModel
     */
    public function deleteList($data)
    {
        return new JsonModel(parent::deleteList($data));
    }

    /**
     * Return single resource
     *
     * @param mixed $id
     * @return JsonModel
     */
    public function get($id)
    {
        return new JsonModel(parent::get($id));
    }

    /**
     * Return list of resources
     *
     * @return JsonModel
     */
    public function getList()
    {
        return new JsonModel(parent::getList());
    }

    /**
     * Retrieve HEAD metadata for the resource
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param  null|mixed $id
     * @return JsonModel
     */
    public function head($id = null)
    {
        return new JsonModel(parent::head($id));
    }

    /**
     * Respond to the OPTIONS method
     *
     * Typically, set the Allow header with allowed HTTP methods, and
     * return the response.
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @return JsonModel
     */
    public function options()
    {
        return new JsonModel(parent::options());
    }

    /**
     * Respond to the PATCH method
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param $id
     * @param $data
     * @return JsonModel
     */
    public function patch($id, $data)
    {
        return new JsonModel(parent::patch($id, $data));
    }

    /**
     * Replace an entire resource collection
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param mixed $data
     * @return JsonModel
     */
    public function replaceList($data)
    {
        return new JsonModel(parent::replaceList($data));
    }

    /**
     * Modify a resource collection without completely replacing it
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.2.0); instead, raises an exception if not implemented.
     *
     * @param mixed $data
     * @return JsonModel
     */
    public function patchList($data)
    {
        return new JsonModel(parent::patchList($data));
    }

    /**
     * Update an existing resource
     *
     * @param mixed $id
     * @param mixed $data
     * @return JsonModel
     */
    public function update($id, $data)
    {
        return new JsonModel(parent::update($id, $data));
    }

    /**
     * Basic functionality for when a page is not available
     *
     * @return JsonModel
     */
    public function notFoundAction()
    {
        return new JsonModel(parent::notFoundAction());
    }

    protected function process(\Closure $closure)
    {
        $className = static::RESOURCE_INTERFACE;
        try {
            $result = $closure();
            if ($result instanceof $className) {
                $result = (new ClassMethods(false))->extract($result);
            }
        } catch (BaseException $e) {
            $result = ['errors' => [$e->getErrors()]];
        } catch (\Exception $e) {
            $result = ['errors' => ['internal' => DEBUG ? $e->getMessage() : '']];
        }
        return new JsonModel($result);
    }
}