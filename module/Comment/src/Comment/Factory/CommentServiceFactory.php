<?php

namespace Comment\Factory;

use Comment\Mapper\CommentMapperInterface;
use Comment\Service\CommentService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CommentServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new CommentService($serviceLocator->get(CommentMapperInterface::class));
    }
}