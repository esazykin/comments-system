<?php

namespace Comment;

use Comment\Mapper\CommentMapperInterface;
use Comment\Service\CommentServiceInterface;

return [
    'service_manager' => [
        'factories' => [
            CommentServiceInterface::class =>  Factory\CommentServiceFactory::class,
            CommentMapperInterface::class => Factory\CommentMapperFactory::class,
        ]
    ],
    'controllers' => [
        'factories' => [
            'Comment\Controller\Comment' => Factory\CommentControllerFactory::class,
        ],
//        'invokables' => [
//            'Comment\Controller\Comment' => Controller\CommentController::class
//        ],
    ],
    'router' => [
        'routes' => [
            'comment' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/api/comment[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => 'Comment\Controller\Comment',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];