import angular from 'angular';
import 'angular-route';
import 'angular-resource';
import 'angular-sanitize';

import './directives';
import controllers from './controllers';
import services from './services';
import filters from './filters';
import factories from './factories';

import routeConfig from './config/route';
import { exception, compileProvider} from 'angular-es6';

export const name = 'app';

export default angular
  .module(name, ['ngRoute', 'ngResource', 'ngSanitize',
    filters, factories, services, controllers])
  .config(exception)
  .config(compileProvider)
  .config(routeConfig);
