export default ['$resource', function ($resource) {
    return $resource('/api/comment/:id', {id: '@id'}, {
            getList: {
                isArray: true
            }
        }
    );
}];