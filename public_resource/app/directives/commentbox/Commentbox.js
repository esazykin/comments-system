import template from './commentbox.html';
import './commentbox.less';
import {Inject} from 'angular-es6';

export default class CommentBox extends Inject {
    static $inject = ['comment'];

    sortModes = [
        {name: "Old at the top", id: 0 },
        {name: "Of the last minute", id: 1 },
        {name: "Random order", id: 2 }
    ];

    constructor(...args) {
        super(...args);

        this.template = template;
        this.restrict = 'E';
    }

    link(scope) {
        const { comment } = this.$inject;

        scope.sortModes = this.sortModes;
        scope.order = this.sortModes[0];

        scope.selectOrder = () => {
            console.log(scope.order);
            scope.comments = comment.getList({mode: scope.order.id});
        };
        scope.resetErrors = () => {
            scope.errors = {common: null, title: null, text: null};
        };
        scope.save = form => {
            let newComment = new comment();
            Object.assign(newComment, scope.newComment);
            newComment.$save(resp => {
                console.log(resp);
                if (resp.errors && Array.isArray(resp.errors)) {
                    resp.errors.forEach(v => CommentBox._dispatchErrors(scope, v));
                    return;
                }
                CommentBox.refresh(scope, comment)
            });
        };
        CommentBox.refresh(scope, comment);
    }

    static refresh(scope) {
        scope.newComment = {};
        scope.selectOrder();
        scope.resetErrors();
    }

    static _dispatchErrors(scope, error) {
        console.log(error);
        if (error.internal) {
            scope.errors.common = 'Sorry. Try again later.';
            return;
        }
        if (!error.external) {
            return;
        }
        if (Array.isArray(error.external)) {
            scope.errors.common = error.external.join('. ');
            return;
        }

        Object.keys(error.external).forEach(field => {
            scope.errors[field] = Object.keys(error.external[field])
                .map(eKey => error.external[field][eKey])
                .join('. ')
        });
    }
}
