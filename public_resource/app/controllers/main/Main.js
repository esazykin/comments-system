import {Inject} from 'angular-es6';

export default class Main extends Inject {
    static $inject = ['$scope'];

    constructor(...args) {
        super(...args);

        this._prepare();
    }

    _prepare() {
        const { $scope } = this.$inject;
    }
}
