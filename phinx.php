<?php

$conf = require('./config/autoload/local.php');

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'production' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'name' => 'comments_system',
            'user' => $conf['db']['username'],
            'pass' => $conf['db']['password'],
            'port' => '3306',
            'charset' => 'utf8',
        ],
        'development' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'name' => 'comments_system',
            'user' => $conf['db']['username'],
            'pass' => $conf['db']['password'],
            'port' => '3306',
            'charset' => 'utf8',
        ],
    ]
];